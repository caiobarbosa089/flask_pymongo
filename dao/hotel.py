import json
from bson.objectid import ObjectId
from bson.json_util import dumps
from extensions import mongo

class HotelDao():
    # Get all hotels
    def getHotels():
        return json.loads(dumps( mongo.db.hotel.find() ))

    # Get hotel by id
    def getHotelById(id):
        try:
            return json.loads(dumps( mongo.db.hotel.find_one({'_id': ObjectId( id )}) ))
        except:
            return None

    # Save a new hotel
    def saveHotel(data):
        return mongo.db.hotel.insert_one(data)

    # Update hotel
    def updateHotel(id, data):
        return mongo.db.hotel.find_one_and_update({'_id': ObjectId( id )}, {"$set": data})

    # Delete hotel
    def deleteHotel(id):
        return mongo.db.hotel.find_one_and_delete({'_id': ObjectId( id )})
