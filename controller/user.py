from dao.user import UserDao

class UserController():
    '''
    For route /users
    '''
    # Get all users
    def getAllUsers():
        # Get all users
        users = UserDao.getUsers()
        # Verify if exists users
        if(users):
            return users, 200
        return { 'messsage': 'User not found' }, 404

    # Update user
    def updateUser(data):
        # Get user id
        user_id = data['id']
        # Get user data
        data = { 'login': data['login'], 'pwd': data['pwd'] }
        # Get user by id
        user = UserDao.getUserById(user_id)
        # Verify if exist user
        if(user):
            # Update user
            user = UserDao.updateUser(user_id, data)
            return { 'message': 'Updated', 'id': str(user_id) }, 200
        return {'message': 'User not found.'}, 404

    '''
    For route /user/<string:user_id>
    '''
    # Get user by id
    def getUserById(user_id):
        # Get user by id
        users = UserDao.getUserById(user_id)
        # Verify if exist user
        if(users):
            return users
        return {'message': 'User not found.'}, 404

    # Delete user by id
    def deleteUser(user_id):
        # Get user by id
        user = UserDao.getUserById(user_id)
        # Verify if exist user
        if(user):
            # Delete user
            user = UserDao.deleteUser(user_id)
            return { 'message': 'Deleted'}, 200
        return {'message': 'User not found.'}, 404

    '''
    For route /user/by-name/<string:user_name>
    '''
    # Get user by name
    def getUserByName(user_name):
        # Get user by name
        users = UserDao.getUserByName(user_name)
        # Verify if exists users
        if(users):
            return users
        return {'message': 'User not found.'}, 404