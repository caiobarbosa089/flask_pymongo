from flask_jwt_extended import create_access_token, get_raw_jwt
from werkzeug.security import safe_str_cmp
from bson.objectid import ObjectId
from blacklist import BLACKLIST 
from dao.user import UserDao

class AuthController():
    '''
    For route /login
    '''
    # Login user
    def login(data):
        # Get user by login
        user = UserDao.getUserByLogin(data['login'])
        # Parse mongo id to string
        _id = str(user['_id'])
        # Compare password
        if(user and safe_str_cmp(user['pwd'], data['pwd'])):
            # Create token
            access_token = create_access_token(identity=_id)
            return {'access_token': access_token}, 200
        return {'message': 'The username or password is incorrect.'}, 401
    
    '''
    For route /logout
    '''
    # Logout user
    def logout():
        # Get jwt
        jwt_id = get_raw_jwt()['jti']
        # Append in the blacklist
        BLACKLIST.add(jwt_id)
        return {'message': 'Logged out successfully!'}, 200

    '''
    For route /register
    '''
    # Register (create a new user
    def register(data):
        # Get user by login
        user = UserDao.getUserByLogin(data['login'])
        # Verify if exists users
        if(user):
            return {"message": "The login '{}' already exists.".format(data['login'])}, 400
        # Create a new user
        UserDao.saveUser(data)
        # Parse return from mongo to dict
        data['_id'] = str(data['_id'])
        return { 'message': 'Inserted', 'data': data }, 201