from flask_restx import Resource, reqparse, fields
from flask_jwt_extended import jwt_required
from controller.hotel import HotelController
from settings import api

# Swagger namespace
hotel_namespace = api.namespace('hotel', description='Hotel operations')

# Swagger models 
hotel_model = api.model('Hotel', {
    'id': fields.String(),
    'name': fields.String(),
    'stars': fields.Float(),
    'daily': fields.Float(),
    'city': fields.String()
})
hotel_model_without_id = api.model('Hotel', {
    'name': fields.String(),
    'stars': fields.Float(),
    'daily': fields.Float(),
    'city': fields.String()
})

# Request body
body_attributes = reqparse.RequestParser()
body_attributes.add_argument('id', type=str)
body_attributes.add_argument('name', type=str)
body_attributes.add_argument('star', type=float)

'''
Route for /hotels
'''
@hotel_namespace.route('') # Swagger namespace
class Hotels(Resource):
    # Get all hotels
    def get(self):
        # Call controller
        return HotelController.getAllHotels()

    # Create new hotel
    @jwt_required # Necessary JWT
    def post(self):
        # Parse body data
        data = body_attributes.parse_args()
        # Call controller
        return HotelController.postNewHotel(data)

    # Update hotel
    @jwt_required # Necessary JWT
    def put(self):
        # Parse body data
        data = body_attributes.parse_args()
        # Call controller
        return HotelController.updateHotel(data)

'''
Route for /hotels/<string:hotel_id>
'''
@api.doc(params={'hotel_id': 'Hotel ID'}) # Params in swagger
@hotel_namespace.route('/<string:hotel_id>') # Swagger namespace
class Hotel(Resource):
    # Get hotel by Id
    def get(self, hotel_id):
        # Call controller
        return HotelController.getHotelById(hotel_id)

    # Delete hotel by id
    @jwt_required # Necessary JWT
    def delete(self, hotel_id):
        # Call controller
        return HotelController.deleteHotel(hotel_id)
